class Show < ActiveRecord::Base
	belongs_to :user
	def getEpisodeList
		self.name

		response = HTTParty.get(URI.encode("http://api.tvmaze.com/singlesearch/shows?q=\"#{self.name}\""))

		id = response["id"]

		episodes = HTTParty.get(URI.encode("http://api.tvmaze.com/shows/#{id}/episodes"))

		episode_list = Array.new
		
		episodes.each do |episode|
			episode_data = Hash.new

			episode_data["season_number"] = episode["season"]
			episode_data["episode_number"] = episode["number"]
			episode_data["episode_name"]  = episode["name"]
			episode_data["episode_airdate"] = episode["airdate"]

			episode_list.push episode_data
		end 
		
		return episode_list
	end

	def getShowInfo
		

		response = HTTParty.get(URI.encode("http://api.tvmaze.com/singlesearch/shows?q=\"#{self.name}\""))

		id = response["id"]

		episodes = HTTParty.get(URI.encode("http://api.tvmaze.com/shows/#{id}/episodes"))

		show_info = Hash.new
		show_info["name"] = response["name"]
		show_info["image"] = response["image"]["medium"]
		show_info["episodes"] = Array.new
		
		episodes.each do |episode|
			episode_data = Hash.new

			episode_data["season_number"] = episode["season"]
			episode_data["episode_number"] = episode["number"]
			episode_data["episode_name"]  = episode["name"]
			episode_data["episode_airdate"] = episode["airdate"]

			episode = episode_data["episode_number"].to_s.length == 1 ? "0#{episode_data["episode_number"]}" : episode_data["episode_number"]
			season = episode_data["season_number"].to_s.length == 1 ? "0#{episode_data["season_number"]}" : episode_data["season_number"]

			episode_data["torrent_search"] = URI::encode("https://thepiratebay.org/search/#{self.name.tr("'","")} s#{season}e#{episode}")
			episode_data["subtitle_search"] = "http://www.tusubtitulo.com/showsub.php?ushow=#{self.name.downcase.tr(" ", "-").tr("'","")}&useason=#{season}&uepisode=#{episode}"

			show_info["episodes"].push episode_data
		end 
		
		show_info["episodes"].select! {|episode| episode["season_number"] >= self.last_season_number and episode["episode_number"] >= self.last_episode_number}
		return show_info
	end

	def getName
		response = HTTParty.get(URI.encode("http://api.tvmaze.com/singlesearch/shows?q=\"#{self.name}\""))

		response["name"]
	end
end
