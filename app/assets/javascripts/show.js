$(document).on("page:change", function() {

	$(document).on("click", "#data tr", function() {
		var edition_enabled = $("#enable_selection").prop('checked');
		if(edition_enabled == true)
		{
			var selected = $(this).hasClass("highlight");
			$("#data tr").removeClass("highlight");
			if(!selected)
				$(this).addClass("highlight");
			var uri = "/shows/";
			var id = $("#id").text();
			uri = uri.concat(id);
			var season_number = $("#data tr.highlight").find(':nth-child(1)').text();
			var episode_number = $("#data tr.highlight").find(':nth-child(2)').text();
			return $.ajax({
				type: "PUT",
				url: uri,
				data: {
					show: {
						last_season_number: season_number,
						last_episode_number: episode_number
					}
				},
				success: function(data)
				{
					$(".highlight").prevAll().each(function(i,e){e.remove()})
					return false;
				},
				error: function(data) {
					return false;
				}
			});
		}

	});

	$(document).on("click", "#reset",function() {
		var selected = $(this).hasClass("highlight");
		$("#data tr").removeClass("highlight");
		if(!selected)
			$(this).addClass("highlight");
		var uri = "/shows/";
		var id = $("#id").text();
		uri = uri.concat(id).concat(".json");
		var season_number = 1;
		var episode_number = 1;
		return $.ajax({
			type: "PUT",
			url: uri,
			data: {
				show: {
					last_season_number: season_number,
					last_episode_number: episode_number
				}
			},
			success: function(data)
			{
				$('#data tbody > tr').remove();
				$(function() {
					$.each(data, function(i, item) {
						console.log(item)
						var $tr = $('<tr>').append(
							$('<td>').text(item.season_number),
							$('<td>').text(item.episode_number),
							$('<td>').text(item.episode_name),
							$('<td>').text(item.episode_airdate)).appendTo('#data tbody');
					});
				});
			},
			error: function(data) {
				return false;
			}
		});
	});

	$(".menu-item").click(function(){
		var id = $(this).data("id");

		var uri = "/shows/";
		uri = uri.concat(id);
		var id = $("#id").text();

		return $.ajax({
			type: "GET",
			url: uri,
			success: function(data){
				$('.show-container').empty();
				$('.show-container').append($.parseHTML(data));
				return false;
			},
			error: function(data) {
				return false;
			}
		});
	});
});