class CreateShows < ActiveRecord::Migration
  def change
    create_table :shows do |t|
      t.string :name
      t.integer :last_episode_number
      t.integer :last_season_number

      t.timestamps null: false
    end
  end
end
