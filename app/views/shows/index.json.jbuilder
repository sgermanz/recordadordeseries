json.array!(@shows) do |show|
  json.extract! show, :id, :name, :last_episode_number, :last_season_number
  json.url show_url(show, format: :json)
end
