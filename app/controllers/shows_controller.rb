class ShowsController < ApplicationController
  before_action :set_show, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!


  def main_index

    @show_list = Array.new

    @shows = Show.all
    @shows.each do |show|
      @show_list.push show.getShowInfo
    end

    return @show_list
  end

  # GET /shows
  # GET /shows.json
  def index
    @shows = current_user.shows
  end

  # GET /shows/1
  # GET /shows/1.json
  def show
    @show_info = @show.getShowInfo
  end

  # GET /shows/new
  def new
    @show = Show.new
  end

  # GET /shows/1/edit
  def edit
  end

  # POST /shows
  # POST /shows.json
  def create
    @show = Show.new(show_params)
    name = @show.getName
    @show.name = name
    @show.user_id = current_user.id

    @show.last_episode_number = 1
    @show.last_season_number = 1

    respond_to do |format|
      if !@show.name.nil? && @show.save
        format.html { redirect_to root_url, notice: "Serie agregada correctamente." }
        format.json { render :show, status: :created, location: @show }
      else
        flash[:notice] = 'Nombre de serie incorrecto.'
        format.html { render :new}
        format.json { render json: @show.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shows/1
  # PATCH/PUT /shows/1.json
  def update
    logger.debug "#{show_params.inspect}"
    respond_to do |format|
      if @show.update(show_params)
        format.html { render :edit, notice: 'Show was successfully updated.' }
        format.json { render json: @show.getEpisodeList, status: :ok  }
      else
        format.html { render :edit }
        format.json { render json: @show.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shows/1
  # DELETE /shows/1.json
  def destroy
    @show.destroy
    respond_to do |format|
      format.html { redirect_to shows_url, notice: 'Serie borrada correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_show
      @show = Show.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def show_params
      params.require(:show).permit(:name, :last_episode_number, :last_season_number)
    end
end
